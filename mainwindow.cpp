#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    bodyLayout = new QHBoxLayout(ui->bodyWidget);
    bodyLayout->setContentsMargins(0,0,0,0);





    setLeftMenu();
    setPreProcBody();

    bodyLayout->addWidget(tabsPreProc);

    drawCon->setConstruction(&con);


    ui->mainToolBar->addAction(QIcon(":/resource/open.png"),"Открыть",this,SLOT(openFile()));
    ui->mainToolBar->addAction(QIcon(":/resource/save.png"),"Сохранить", this, SLOT(saveFile()));
    ui->mainToolBar->addAction(QIcon(":/resource/about.png"),"О программе",this,SLOT(aboutProgramm()));



}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::setLeftMenu()
{

}

void MainWindow::setPreProcBody()
{
    tabsPreProc = new QTabWidget(ui->bodyWidget);
    QWidget *tab1 = new QWidget(tabsPreProc);
    QVBoxLayout *tab1Layout = new QVBoxLayout(tab1);



    drawCon = new Painter(tab1);
    tab1Layout->addWidget(drawCon);

    QCheckBox *drawNum = new QCheckBox(tab1);
    drawNum->setText("Рисовать номера стержней и узлов");
    drawNum->setChecked(false);
    connect(drawNum, SIGNAL(clicked(bool)),drawCon,SLOT(setDrawNumber(bool)));
    tab1Layout->addWidget(drawNum);

    QHBoxLayout *paramLayout = new QHBoxLayout(tab1);
    tab1Layout->addLayout(paramLayout);

    QCheckBox *drawConForce = new QCheckBox(tab1);
    drawConForce->setText("Рисовать сосредоточенные нагрузки");
    drawConForce->setChecked(false);
    connect(drawConForce, SIGNAL(clicked(bool)),drawCon,SLOT(setDrawConForce(bool)));
    paramLayout->addWidget(drawConForce);

    QCheckBox *drawDistForce = new QCheckBox(tab1);
    drawDistForce->setText("Рисовать распределенные нагрузки");
    drawDistForce->setChecked(false);
    connect(drawDistForce, SIGNAL(clicked(bool)),drawCon,SLOT(setDrawDistForce(bool)));
    paramLayout->addWidget(drawDistForce);

    tabsPreProc->addTab(tab1, "Конструкция");

    tab2 = new QWidget(tabsPreProc);
    setConTab2();
    tabsPreProc->addTab(tab2, "Файл конструкции");

    tab3 = new QWidget(tabsPreProc);
    setConTab3();
    tabsPreProc->addTab(tab3, "Файл нагрузок");

    connect(tabsPreProc, SIGNAL(currentChanged(int)),this,SLOT(tabChange(int)));
}

void MainWindow::setConTab2()
{
    QVBoxLayout *tabLayout = new QVBoxLayout(tab2);
    tabLayout->setContentsMargins(2,2,2,2);
    QScrollArea *scrollArea = new QScrollArea(tab2);

    tab2Body = new QWidget(tab2);
    scrollArea->setWidget(tab2Body);
    tab2Body->resize(500,760);
    scrollArea->setStyleSheet("QScrollArea { background: rgba(255, 255, 255 , 0);}");
    scrollArea->setFrameStyle(QFrame::NoFrame);
    QPalette pal(palette());
    pal.setColor(QPalette::Background, QColor(0,0,0,0));
    tab2Body->setAutoFillBackground(true);
    tab2Body->setPalette(pal);


    QLabel *nodesLabel = new QLabel(tab2Body);
    nodesLabel->move(5,5);
    nodesLabel->setFont(QFont("Arial", 15));
    nodesLabel->setText("Редактирование узлов:");


    nodesTable = new QTableWidget(tab2Body);
    nodesTable->setGeometry(5,40,300,200);
    nodesTable->setRowCount(0);
    nodesTable->setColumnCount(2);
    QStringList sl;
    sl.push_back("X");
    sl.push_back("Y");
    nodesTable->setHorizontalHeaderLabels(sl);
    nodesTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    addNode = new QPushButton(tab2Body);
    addNode->setText("Добавить узел");
    addNode->setGeometry(320,40,170,30);
    connect(addNode, SIGNAL(clicked()), this, SLOT(addNodeButtonClicked()));

    removeNode = new QPushButton(tab2Body);
    removeNode->setText("Удалить узел");
    removeNode->setGeometry(320,80,170,30);
    connect(removeNode, SIGNAL(clicked()), this, SLOT(removeNodeButtonClicked()));



    QLabel *rodLabel = new QLabel(tab2Body);
    rodLabel->move(5,260);
    rodLabel->setFont(QFont("Arial", 15));
    rodLabel->setText("Редактирование стержней:");


    rodTable = new QTableWidget(tab2Body);
    rodTable->setGeometry(5,295,300,200);
    rodTable->setRowCount(0);
    rodTable->setColumnCount(3);
    QStringList sl2;
    sl2.push_back("1 точка");
    sl2.push_back("2 точка");
    sl2.push_back("Тип");
    rodTable->setHorizontalHeaderLabels(sl2);
    rodTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    addRod = new QPushButton(tab2Body);
    addRod->setText("Добавить стержень");
    addRod->setGeometry(320,295,170,30);
    connect(addRod, SIGNAL(clicked()), this, SLOT(addRodButtonClicked()));

    removeRod = new QPushButton(tab2Body);
    removeRod->setText("Удалить стержень");
    removeRod->setGeometry(320,335,170,30);
    connect(removeRod, SIGNAL(clicked()), this, SLOT(removeRodButtonClicked()));


    QLabel *rodTypeLabel = new QLabel(tab2Body);
    rodTypeLabel->move(5,515);
    rodTypeLabel->setFont(QFont("Arial", 15));
    rodTypeLabel->setText("Редактирование типов стержней:");

    rodTypeTable = new QTableWidget(tab2Body);
    rodTypeTable->setGeometry(5,550,300,200);
    rodTypeTable->setRowCount(0);
    rodTypeTable->setColumnCount(3);
    QStringList sl3;
    sl3.push_back("E");
    sl3.push_back("A");
    sl3.push_back("Sp");
    rodTypeTable->setHorizontalHeaderLabels(sl3);
    rodTypeTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    addRodType = new QPushButton(tab2Body);
    addRodType->setText("Добавить тип стержня");
    addRodType->setGeometry(320,550,170,30);
    connect(addRodType, SIGNAL(clicked()), this, SLOT(addRodTypeButtonClicked()));

    removeRodType = new QPushButton(tab2Body);
    removeRodType->setText("Удалить тип стерженя");
    removeRodType->setGeometry(320,590,170,30);
    connect(removeRodType, SIGNAL(clicked()), this, SLOT(removeRodTypeButtonClicked()));

    updateConTab2();

    tabLayout->addWidget(scrollArea);
}

void MainWindow::updateConTab2()
{
    nodesTable->clear();
    nodesTable->setRowCount(0);

    QStringList sl;
    sl.push_back("X");
    sl.push_back("Y");
    nodesTable->setHorizontalHeaderLabels(sl);

    for(int i = 0; i<con.nodes.size(); i++)
    {
        nodesTable->insertRow(i);

        QTableWidgetItem *i1 = new QTableWidgetItem("X",0);
        i1->setData(0,con.nodes[i].x);
        nodesTable->setItem(i,0,i1);

        QTableWidgetItem *i2 = new QTableWidgetItem("Y",0);
        i2->setData(0,con.nodes[i].y);
        nodesTable->setItem(i,1,i2);
    }

    rodTable->clear();
    rodTable->setRowCount(0);

    QStringList sl2;
    sl2.push_back("1 точка");
    sl2.push_back("2 точка");
    sl2.push_back("Тип");
    rodTable->setHorizontalHeaderLabels(sl2);

    for(int i = 0; i<con.rods.size(); i++)
    {
        rodTable->insertRow(i);

        QTableWidgetItem *i1 = new QTableWidgetItem("n1",0);
        i1->setData(0,con.rods[i].n1);
        rodTable->setItem(i,0,i1);

        QTableWidgetItem *i2 = new QTableWidgetItem("n2",0);
        i2->setData(0,con.rods[i].n2);
        rodTable->setItem(i,1,i2);

        QTableWidgetItem *i3 = new QTableWidgetItem("Type",0);
        i3->setData(0,con.rods[i].type);
        rodTable->setItem(i,2,i3);
    }


    rodTypeTable->clear();
    rodTypeTable->setRowCount(0);

    QStringList sl3;
    sl3.push_back("E");
    sl3.push_back("A");
    sl3.push_back("Sp");
    rodTypeTable->setHorizontalHeaderLabels(sl3);

    for(int i = 0; i<con.rodTypes.size(); i++)
    {
        rodTypeTable->insertRow(i);

        QTableWidgetItem *i1 = new QTableWidgetItem("n1",0);
        i1->setData(0,con.rodTypes[i].E);
        rodTypeTable->setItem(i,0,i1);

        QTableWidgetItem *i2 = new QTableWidgetItem("n2",0);
        i2->setData(0,con.rodTypes[i].A);
        rodTypeTable->setItem(i,1,i2);

        QTableWidgetItem *i3 = new QTableWidgetItem("Type",0);
        i3->setData(0,con.rodTypes[i].Sp);
        rodTypeTable->setItem(i,2,i3);
    }
}

void MainWindow::setConTab3()
{
    QVBoxLayout *tab3Layout = new QVBoxLayout(tab3);
    tab3Layout->setContentsMargins(2,2,2,2);
    QScrollArea *scrollArea = new QScrollArea(tab3);

    tab3Body = new QWidget(tab3);
    scrollArea->setWidget(tab3Body);
    tab3Body->resize(600,760);
    scrollArea->setStyleSheet("QScrollArea { background: rgba(255, 255, 255 , 0);}");
    scrollArea->setFrameStyle(QFrame::NoFrame);
    QPalette pal(palette());
    pal.setColor(QPalette::Background, QColor(0,0,0,0));
    tab3Body->setAutoFillBackground(true);
    tab3Body->setPalette(pal);


    QLabel *conLoadLabel = new QLabel(tab3Body);
    conLoadLabel->move(5,5);
    conLoadLabel->setFont(QFont("Arial", 15));
    conLoadLabel->setText("Редактирование сосредоточенных нагрузок:");

    conLoadTable = new QTableWidget(tab3Body);
    conLoadTable->setGeometry(5,40,320,200);
    conLoadTable->setRowCount(0);
    conLoadTable->setColumnCount(3);
    QStringList sl;
    sl.push_back("Узел");
    sl.push_back("Сила");
    sl.push_back("Направление");
    conLoadTable->setHorizontalHeaderLabels(sl);
    conLoadTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);


    addConLoad = new QPushButton(tab3Body);
    addConLoad->setText("Добавить сосредоточенную\n нагрузку");
    addConLoad->setGeometry(340,40,200,50);
    connect(addConLoad, SIGNAL(clicked()), this, SLOT(addConLoadButtonClicked()));

    removeConLoad = new QPushButton(tab3Body);
    removeConLoad->setText("Удалить сосредоточенную\n нагрузку");
    removeConLoad->setGeometry(340,100,200,50);
    connect(removeConLoad, SIGNAL(clicked()), this, SLOT(removeConLoadButtonClicked()));


    QLabel *distLoadLabel = new QLabel(tab3Body);
    distLoadLabel->move(5,260);
    distLoadLabel->setFont(QFont("Arial", 15));
    distLoadLabel->setText("Редактирование распределенных нагрузок:");

    distLoadTable = new QTableWidget(tab3Body);
    distLoadTable->setGeometry(5,295,320,200);
    distLoadTable->setRowCount(0);
    distLoadTable->setColumnCount(3);
    QStringList sl2;
    sl2.push_back("Стержень");
    sl2.push_back("Сила");
    sl2.push_back("Направление");
    distLoadTable->setHorizontalHeaderLabels(sl2);
    distLoadTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    addDistLoad = new QPushButton(tab3Body);
    addDistLoad->setText("Добавить распределенную\n нагрузку");
    addDistLoad->setGeometry(340,295,200,50);
    connect(addDistLoad, SIGNAL(clicked()), this, SLOT(addDistLoadButtonClicked()));

    removeDistLoad = new QPushButton(tab3Body);
    removeDistLoad->setText("Удалить распределенную\n нагрузку");
    removeDistLoad->setGeometry(340,355,200,50);
    connect(removeDistLoad, SIGNAL(clicked()), this, SLOT(removeDistLoadButtonClicked()));


    QLabel *propLabel = new QLabel(tab3Body);
    propLabel->move(5,515);
    propLabel->setFont(QFont("Arial", 15));
    propLabel->setText("Редактирование опор:");

    propTable = new QTableWidget(tab3Body);
    propTable->setGeometry(5,550,320,200);
    propTable->setRowCount(0);
    propTable->setColumnCount(2);
    QStringList sl3;
    sl3.push_back("Узел");
    sl3.push_back("Расположение");
    propTable->setHorizontalHeaderLabels(sl3);
    propTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    addProp = new QPushButton(tab3Body);
    addProp->setText("Добавить опору");
    addProp->setGeometry(340,550,200,50);
    connect(addProp, SIGNAL(clicked()), this, SLOT(addPropButtonClicked()));

    removeProp = new QPushButton(tab3Body);
    removeProp->setText("Удалить опору");
    removeProp->setGeometry(340,610,200,50);
    connect(removeProp, SIGNAL(clicked()), this, SLOT(removePropButtonClicked()));


    updateConTab3();

    tab3Layout->addWidget(scrollArea);
}

void MainWindow::updateConTab3()
{
    conLoadTable->clear();
    conLoadTable->setRowCount(0);

    QStringList sl;
    sl.push_back("Узел");
    sl.push_back("Сила");
    sl.push_back("Направление");
    conLoadTable->setHorizontalHeaderLabels(sl);

    for(int i = 0; i<con.conLoads.size(); i++)
    {
        conLoadTable->insertRow(i);

        QTableWidgetItem *i1 = new QTableWidgetItem("node",0);
        i1->setData(0,con.conLoads[i].node);
        conLoadTable->setItem(i,0,i1);

        QTableWidgetItem *i2 = new QTableWidgetItem("f",0);
        i2->setData(0,con.conLoads[i].f);
        conLoadTable->setItem(i,1,i2);

        QTableWidgetItem *i3 = new QTableWidgetItem("dir",0);
        i3->setData(0,con.conLoads[i].dir);
        conLoadTable->setItem(i,2,i3);
    }


    distLoadTable->clear();
    distLoadTable->setRowCount(0);

    QStringList sl2;
    sl2.push_back("Стержень");
    sl2.push_back("Сила");
    sl2.push_back("Направление");
    distLoadTable->setHorizontalHeaderLabels(sl2);

    for(int i = 0; i<con.distLoads.size(); i++)
    {
        distLoadTable->insertRow(i);

        QTableWidgetItem *i1 = new QTableWidgetItem("rod",0);
        i1->setData(0,con.distLoads[i].rod);
        distLoadTable->setItem(i,0,i1);

        QTableWidgetItem *i2 = new QTableWidgetItem("q",0);
        i2->setData(0,con.distLoads[i].q);
        distLoadTable->setItem(i,1,i2);

        QTableWidgetItem *i3 = new QTableWidgetItem("dir",0);
        i3->setData(0,con.distLoads[i].dir);
        distLoadTable->setItem(i,2,i3);
    }

    propTable->clear();
    propTable->setRowCount(0);

    QStringList sl3;
    sl3.push_back("Узел");
    sl3.push_back("Расположение");
    propTable->setHorizontalHeaderLabels(sl3);

    for(int i = 0; i<con.props.size(); i++)
    {
        propTable->insertRow(i);

        QTableWidgetItem *i1 = new QTableWidgetItem("node",0);
        i1->setData(0,con.props[i].node);
        propTable->setItem(i,0,i1);

        QTableWidgetItem *i2 = new QTableWidgetItem("dir",0);
        i2->setData(0,con.props[i].dir);
        propTable->setItem(i,1,i2);
    }
}

void MainWindow::tabChange(int tab)
{
    if(tab == 1)
    {
        updateConTab2();

    }else{

        for(int i = 0; i<rodTable->rowCount(); i++)
        {
            int count1 = 0;
            int count2 = 0;
            for(int j = 0; j<nodesTable->rowCount(); j++)
            {
                if((rodTable->item(i,0)->data(0).toInt()-1) == j)
                {
                    count1++;
                }
                if((rodTable->item(i,1)->data(0).toInt()-1) == j)
                {
                    count2++;
                }
            }
            if(count1 == 0)
            {
                QMessageBox::warning(this, "Ошибка", "Узла ("+QString::number(rodTable->item(i,0)->data(0).toInt())
                                     +") не существует.");
                return;
            }
            if(count2 == 0)
            {
                QMessageBox::warning(this, "Ошибка", "Узла ("+QString::number(rodTable->item(i,1)->data(0).toInt())
                                     +") не существует.");
                return;
            }
        }


        con.nodes.clear();
        for(int i = 0; i<nodesTable->rowCount(); i++)
        {
            con.addNode(nodesTable->item(i,0)->data(0).toFloat(),
                        nodesTable->item(i,1)->data(0).toFloat());
        }

        con.rods.clear();
        for(int i = 0; i<rodTable->rowCount(); i++)
        {
            con.addRod(rodTable->item(i,0)->data(0).toInt(),
                       rodTable->item(i,1)->data(0).toInt(),
                       rodTable->item(i,2)->data(0).toInt());
        }

        con.rodTypes.clear();
        for(int i = 0; i<rodTypeTable->rowCount(); i++)
        {
            con.addRodType(rodTypeTable->item(i,0)->data(0).toFloat(),
                           rodTypeTable->item(i,1)->data(0).toFloat(),
                           rodTypeTable->item(i,2)->data(0).toFloat());
        }
    }

    if(tab == 2)
    {
        updateConTab3();
    }else{



        for(int i = 0; i<conLoadTable->rowCount(); i++)
        {
            int count = 0;
            for(int j = 0; j<con.nodes.size(); j++)
            {
                if((conLoadTable->item(i,0)->data(0).toInt()-1) == j)
                    count++;
            }

            if(count == 0)
            {
                QMessageBox::warning(this, "Ошибка", "Узла ("+QString::number(conLoadTable->item(i,0)->data(0).toInt())
                                     +") не существует.");
                return;
            }

            for(int j = 0; j<propTable->rowCount(); j++)
            {
                if(conLoadTable->item(i,0)->data(0).toInt() == propTable->item(j,0)->data(0).toInt())
                {
                    QMessageBox::warning(this, "Ошибка", "Один и тот-же узел ("+QString::number(conLoadTable->item(i,0)->data(0).toInt())
                                         +") не может быть опорой и иметь сосредоточенную нагрузку.");
                    return;
                }
            }

        }

        for(int i = 0; i<propTable->rowCount(); i++)
        {
            for(int j = 0; j<propTable->rowCount(); j++)
            {
                if(i != j)
                {
                    if(propTable->item(i,0)->data(0).toInt() == propTable->item(j,0)->data(0).toInt())
                    {
                        QMessageBox::warning(this, "Ошибка", "Один и тот-же узел ("+QString::number(propTable->item(i,0)->data(0).toInt())
                                             +") не может иметь несколько опор.");
                        return;
                    }
                }
            }
        }

        con.conLoads.clear();
        for(int i = 0; i<conLoadTable->rowCount(); i++)
        {
            con.addConLoad(conLoadTable->item(i,0)->data(0).toInt(),
                           conLoadTable->item(i,1)->data(0).toFloat(),
                           conLoadTable->item(i,2)->data(0).toInt());
        }

        con.distLoads.clear();
        for(int i = 0; i<distLoadTable->rowCount(); i++)
        {
            con.addDistLoad(distLoadTable->item(i,0)->data(0).toInt(),
                            distLoadTable->item(i,1)->data(0).toFloat(),
                            distLoadTable->item(i,2)->data(0).toInt());

        }

        con.props.clear();
        for(int i = 0; i<propTable->rowCount(); i++)
        {
            con.addProp(propTable->item(i,0)->data(0).toInt(),
                        propTable->item(i,1)->data(0).toInt());

        }
    }
}

void MainWindow::addNodeButtonClicked()
{
    int row = nodesTable->rowCount();
    nodesTable->insertRow(row);

    QTableWidgetItem *i1 = new QTableWidgetItem("X",0);
    i1->setData(0,0.0);
    nodesTable->setItem(row,0,i1);

    QTableWidgetItem *i2 = new QTableWidgetItem("Y",0);
    i2->setData(0,0.0);
    nodesTable->setItem(row,1,i2);
}

void MainWindow::removeNodeButtonClicked()
{
    nodesTable->removeRow(nodesTable->currentRow());
}

void MainWindow::addRodButtonClicked()
{
    int row = rodTable->rowCount();
    rodTable->insertRow(row);

    QTableWidgetItem *i1 = new QTableWidgetItem("n1",0);
    i1->setData(0,0);
    rodTable->setItem(row,0,i1);

    QTableWidgetItem *i2 = new QTableWidgetItem("n2",0);
    i2->setData(0,0);
    rodTable->setItem(row,1,i2);

    QTableWidgetItem *i3 = new QTableWidgetItem("Type",0);
    i3->setData(0,0);
    rodTable->setItem(row,2,i3);
}

void MainWindow::removeRodButtonClicked()
{
    rodTable->removeRow(rodTable->currentRow());
}

void MainWindow::addRodTypeButtonClicked()
{
    int row = rodTypeTable->rowCount();
    rodTypeTable->insertRow(row);

    QTableWidgetItem *i1 = new QTableWidgetItem("E",0);
    i1->setData(0,0.0);
    rodTypeTable->setItem(row,0,i1);

    QTableWidgetItem *i2 = new QTableWidgetItem("A",0);
    i2->setData(0,0.0);
    rodTypeTable->setItem(row,1,i2);

    QTableWidgetItem *i3 = new QTableWidgetItem("Sp",0);
    i3->setData(0,0.0);
    rodTypeTable->setItem(row,2,i3);
}

void MainWindow::removeRodTypeButtonClicked()
{
    rodTypeTable->removeRow(rodTypeTable->currentRow());
}

void MainWindow::addConLoadButtonClicked()
{
    int row = conLoadTable->rowCount();
    conLoadTable->insertRow(row);

    QTableWidgetItem *i1 = new QTableWidgetItem("Node",0);
    i1->setData(0,0);
    conLoadTable->setItem(row,0,i1);

    QTableWidgetItem *i2 = new QTableWidgetItem("f",0);
    i2->setData(0,0.0);
    conLoadTable->setItem(row,1,i2);

    QTableWidgetItem *i3 = new QTableWidgetItem("dir",0);
    i3->setData(0,0);
    conLoadTable->setItem(row,2,i3);
}

void MainWindow::removeConLoadButtonClicked()
{
    conLoadTable->removeRow(conLoadTable->currentRow());
}

void MainWindow::addDistLoadButtonClicked()
{
    int row = distLoadTable->rowCount();
    distLoadTable->insertRow(row);

    QTableWidgetItem *i1 = new QTableWidgetItem("Rod",0);
    i1->setData(0,0);
    distLoadTable->setItem(row,0,i1);

    QTableWidgetItem *i2 = new QTableWidgetItem("q",0);
    i2->setData(0,0.0);
    distLoadTable->setItem(row,1,i2);

    QTableWidgetItem *i3 = new QTableWidgetItem("dir",0);
    i3->setData(0,0);
    distLoadTable->setItem(row,2,i3);
}

void MainWindow::removeDistLoadButtonClicked()
{
    distLoadTable->removeRow(distLoadTable->currentRow());
}

void MainWindow::addPropButtonClicked()
{
    int row = propTable->rowCount();
    propTable->insertRow(row);

    QTableWidgetItem *i1 = new QTableWidgetItem("node",0);
    i1->setData(0,0);
    propTable->setItem(row,0,i1);

    QTableWidgetItem *i2 = new QTableWidgetItem("dir",0);
    i2->setData(0,0);
    propTable->setItem(row,1,i2);
}

void MainWindow::removePropButtonClicked()
{
    propTable->removeRow(propTable->currentRow());
}

void MainWindow::saveFile()
{
    QString filePath = QFileDialog::getSaveFileName(this, tr("Сохранить проект"),"",tr("Con Files (*.con)"));
    QFile conFile(filePath);
    conFile.open(QIODevice::WriteOnly);

    QFileInfo infoFile(filePath);
    QString dir = infoFile.dir().path();
    QString fName = infoFile.fileName();

    QTextStream wr(&conFile);

    wr << fName << "_construction.cp\n";
    wr << fName << "_force.fp\n";
    conFile.close();

    QFile constructionFile(dir + "/"+fName+"_construction.cp");
    constructionFile.open(QIODevice::WriteOnly);

    QTextStream wConFile(&constructionFile);
    wConFile << "nodes\n";
    wConFile << nodesTable->rowCount() << "\n";
    for(int i = 0; i<nodesTable->rowCount(); i++)
    {
        wConFile << nodesTable->item(i,0)->data(0).toFloat() << "\n";
        wConFile << nodesTable->item(i,1)->data(0).toFloat() << "\n";
    }
    wConFile << "rods\n";
    wConFile << rodTable->rowCount() << "\n";
    for(int i = 0; i<rodTable->rowCount(); i++)
    {
        wConFile << rodTable->item(i,0)->data(0).toInt() << "\n";
        wConFile << rodTable->item(i,1)->data(0).toInt() << "\n";
        wConFile <<  rodTable->item(i,2)->data(0).toInt() << "\n";
    }
    wConFile << "rod_types\n";
    wConFile << rodTypeTable->rowCount() << "\n";
    for(int i = 0; i<rodTypeTable->rowCount(); i++)
    {
        wConFile << rodTypeTable->item(i,0)->data(0).toFloat() << "\n";
        wConFile << rodTypeTable->item(i,1)->data(0).toFloat() << "\n";
        wConFile << rodTypeTable->item(i,2)->data(0).toFloat() << "\n";
    }
    constructionFile.close();


    QFile forceFile(dir + "/"+fName+"_force.fp");
    forceFile.open(QIODevice::WriteOnly);
    QTextStream wForceFile(&forceFile);
    wForceFile << "con_loads\n";
    wForceFile << conLoadTable->rowCount() << "\n";
    for(int i = 0; i<conLoadTable->rowCount(); i++)
    {
        wForceFile << conLoadTable->item(i,0)->data(0).toInt() << "\n";
        wForceFile << conLoadTable->item(i,1)->data(0).toFloat() << "\n";
        wForceFile << conLoadTable->item(i,2)->data(0).toInt() << "\n";
    }
    wForceFile << "dist_loads\n";
    wForceFile << distLoadTable->rowCount() << "\n";
    for(int i = 0; i<distLoadTable->rowCount(); i++)
    {
        wForceFile << distLoadTable->item(i,0)->data(0).toInt() << "\n";
        wForceFile << distLoadTable->item(i,1)->data(0).toFloat() << "\n";
        wForceFile << distLoadTable->item(i,2)->data(0).toInt() << "\n";

    }
    wForceFile << "props\n";
    wForceFile << propTable->rowCount() << "\n";
    for(int i = 0; i<propTable->rowCount(); i++)
    {
        wForceFile << propTable->item(i,0)->data(0).toInt() << "\n";
        wForceFile << propTable->item(i,1)->data(0).toInt() << "\n";

    }
    forceFile.close();
}

void MainWindow::openFile()
{
    QString filePath = QFileDialog::getOpenFileName(this, tr("Открыть проект"), "", tr("Con Files (*.con)"));


    QFile conFile(filePath);
    conFile.open(QIODevice::ReadOnly);

    QFileInfo infoFile(filePath);
    QString dir = infoFile.dir().path();

    QTextStream r(&conFile);

    QFile constructionFile(dir+"/"+r.readLine());
    constructionFile.open(QIODevice::ReadOnly);

    QTextStream readConstructionFile(&constructionFile);

    qDebug() << readConstructionFile.readLine();
    con.nodes.clear();
    int size = readConstructionFile.readLine().toInt();

    for(int i = 0; i<size; i++)
    {
        float x = readConstructionFile.readLine().toFloat();
        float y = readConstructionFile.readLine().toFloat();
        con.addNode(x,y);
    }

    qDebug() << readConstructionFile.readLine();
    con.rods.clear();
    size = readConstructionFile.readLine().toInt();
    for(int i = 0; i<size; i++)
    {
        int n1 = readConstructionFile.readLine().toInt();
        int n2 = readConstructionFile.readLine().toInt();
        int type = readConstructionFile.readLine().toInt();
        con.addRod(n1, n2, type);
    }

    qDebug() << readConstructionFile.readLine();
    con.rodTypes.clear();
    size = readConstructionFile.readLine().toInt();
    for(int i = 0; i<size; i++)
    {
        float E = readConstructionFile.readLine().toFloat();
        float A = readConstructionFile.readLine().toFloat();
        float Sp = readConstructionFile.readLine().toFloat();
        con.addRodType(E, A, Sp);
    }

    constructionFile.close();


    QFile forceFile(dir+"/"+r.readLine());
    forceFile.open(QIODevice::ReadOnly);


    QTextStream readForceFile(&forceFile);

    qDebug() << readForceFile.readLine();
    con.conLoads.clear();
    size = readForceFile.readLine().toInt();
    for(int i = 0; i<size; i++)
    {
        int node = readForceFile.readLine().toInt();
        float f = readForceFile.readLine().toFloat();
        int dir =readForceFile.readLine().toInt();
        con.addConLoad(node, f, dir);
    }

    qDebug() << readForceFile.readLine();
    con.distLoads.clear();
    size = readForceFile.readLine().toInt();
    for(int i = 0; i<size; i++)
    {
        int rod = readForceFile.readLine().toInt();
        float q = readForceFile.readLine().toFloat();
        int dir = readForceFile.readLine().toInt();
        con.addDistLoad(rod, q, dir);

    }

    qDebug() << readForceFile.readLine();
    con.props.clear();
    size = readForceFile.readLine().toInt();
    for(int i = 0; i<size; i++)
    {
        int node = readForceFile.readLine().toInt();
        int dir = readForceFile.readLine().toInt();
        con.addProp(node, dir);

    }

    forceFile.close();

    conFile.close();

    updateConTab2();
    updateConTab3();
    drawCon->repaint();
}

void MainWindow::aboutProgramm()
{
    QMessageBox::information(this, "О программе", "Автор Саня Журавлев, копирайт, все дела, СТАНКИН, 2013");
}

void MainWindow::resizeEvent(QResizeEvent *e)
{
    // tab2Body->resize(tab2->width()-50,1000);
}
