#-------------------------------------------------
#
# Project created by QtCreator 2013-10-30T10:21:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SOPROMAT
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    painter.cpp \
    construction.cpp

HEADERS  += mainwindow.h \
    painter.h \
    construction.h

FORMS    += mainwindow.ui

RESOURCES += \
    resource.qrc
