#ifndef PAINTER_H
#define PAINTER_H

#include <QWidget>
#include <QPainter>
#include <QKeyEvent>
#include <QFontMetrics>
#include <QImage>
#include <vector>
#include <QDebug>

#include "construction.h"

class Painter : public QWidget
{
    Q_OBJECT

    float scale;

    int ofx;
    int ofy;

    bool drawN;
    bool drawConF;
    bool drawDistF;

    Construction *con;

public:
    explicit Painter(QWidget *parent = 0);

    void setConstruction(Construction *con);
public slots:
    void setDrawNumber(bool d);
    void setDrawConForce(bool d);
    void setDrawDistForce(bool d);
protected:
    void paintEvent(QPaintEvent *e);
    void keyPressEvent(QKeyEvent *k);
    void mousePressEvent(QMouseEvent *);

};

#endif // PAINTER_H
