#ifndef CONSTRUCTION_H
#define CONSTRUCTION_H

#include <vector>

struct Node
{
    float x;
    float y;
};

struct RodType
{
   float E;
   float A;
   float Sp;
};

struct Rod
{
    int n1;
    int n2;
    int type;
};

struct ConLoad
{
    int node;
    float f;
    int dir;
};

struct DistLoad
{
    int rod;
    float q;
    int dir;
};

struct Prop
{
    int node;
    int dir;
};


class Construction
{

public:
    Construction();

    void addNode(int x, int y);
    void addRod(int n1, int n2, int type);
    void addRodType(float E, float A, float Sp);
    void addProp(int node, int dir);

    std::vector<Node> nodes;
    std::vector<RodType> rodTypes;
    std::vector<Rod> rods;
    std::vector<Prop> props;

    void addConLoad(int node, float f, int dir);
    void addDistLoad(int rod, float q, int dir);

    std::vector<ConLoad> conLoads;
    std::vector<DistLoad> distLoads;
};

#endif // CONSTRUCTION_H
