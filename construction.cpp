#include "construction.h"

Construction::Construction()
{
}

void Construction::addNode(int x, int y)
{
    Node node;
    node.x = x;
    node.y = y;

    nodes.push_back(node);
}

void Construction::addRod(int n1, int n2, int type)
{
    Rod rod;
    rod.n1 = n1;
    rod.n2 = n2;

    rod.type = type;

    rods.push_back(rod);
}

void Construction::addRodType(float E, float A, float Sp)
{
    RodType rodType;
    rodType.E = E;
    rodType.A = A;
    rodType.Sp = Sp;

    rodTypes.push_back(rodType);

}

void Construction::addProp(int node, int dir)
{
    Prop prop;
    prop.node = node;
    prop.dir = dir;
    props.push_back(prop);
}

void Construction::addConLoad(int node, float f, int dir)
{
    ConLoad conLoad;
    conLoad.node = node;
    conLoad.f = f;
    conLoad.dir = dir;
    conLoads.push_back(conLoad);
}

void Construction::addDistLoad(int rod, float q, int dir)
{
    DistLoad distLoad;
    distLoad.rod = rod;
    distLoad.q = q;
    distLoad.dir = dir;
    distLoads.push_back(distLoad);
}
