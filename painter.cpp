#include "painter.h"
#include <qmath.h>
#include <math.h>

Painter::Painter(QWidget *parent) :
    QWidget(parent)
{
    setFocus();
    scale = 10;
    ofx = 0;
    ofy = 0;
    drawN = false;
    drawConF = false;
    drawDistF = false;
}

void Painter::setConstruction(Construction *con)
{
    this->con = con;
}

void Painter::setDrawNumber(bool d)
{
    drawN = d;
    repaint();
}

void Painter::setDrawConForce(bool d)
{
    drawConF = d;
    repaint();
}

void Painter::setDrawDistForce(bool d)
{
    drawDistF = d;
    repaint();
}

void Painter::paintEvent(QPaintEvent *e)
{
    QPainter p(this);

    p.setRenderHint(QPainter::Antialiasing, true);
    //p.setRenderHint(QPainter::HighQualityAntialiasing);



    p.setBrush(QBrush(QColor(0,0,0)));
    p.drawRect(0,0,width(), height());


    int cx = width()/2+ofx;
    int cy = height()/2+ofy;


    p.setPen(QPen(QColor(255,255,255,100)));
    p.drawLine(cx,0,cx,height());
    p.drawLine(0,cy,width(),cy);



    float lx = 1;
    for(int i = cx+10*scale; i<width(); i+=10*scale)
    {
        p.setPen(QPen(QColor(255,255,255,50)));
        p.drawLine(i,0,i,height());
        p.setFont(QFont("Arial",2.6*scale));
        QFontMetrics fm(QFont("Arial",2.6*scale));
        p.setPen(QPen(QColor(255,255,255,50)));
        p.drawText(i-fm.width(QString::number(lx+=1))/2, cy+3.2*scale, QString::number(lx));
    }

    lx = -1;
    for(int i = cx-10*scale; i>0; i-=10*scale)
    {
        p.setPen(QPen(QColor(255,255,255,50)));
        p.drawLine(i,0,i,height());
        p.setFont(QFont("Arial",2.6*scale));
        QFontMetrics fm(QFont("Arial",2.6*scale));
        p.setPen(QPen(QColor(255,255,255,50)));
        p.drawText(i-fm.width(QString::number(lx-=1))/2, cy+3.2*scale, QString::number(lx));
    }

    float ly = -1;
    for(int i = cy+10*scale; i<height(); i+=10*scale)
    {
        p.setPen(QPen(QColor(255,255,255,50)));
        p.drawLine(0,i,width(),i);
        p.setFont(QFont("Arial",2.6*scale));
        QFontMetrics fm(QFont("Arial",2.6*scale));
        p.setPen(QPen(QColor(255,255,255,50)));
        p.drawText(cx-fm.width(QString::number(ly-=1))-2, i+2.6*scale/2 , QString::number(ly));
    }

    ly = 1;
    for(int i = cy-10*scale; i>0; i-=10*scale)
    {
        p.setPen(QPen(QColor(255,255,255,50)));
        p.drawLine(0,i,width(),i);
        p.setFont(QFont("Arial",2.6*scale));
        QFontMetrics fm(QFont("Arial",2.6*scale));
        p.setPen(QPen(QColor(255,255,255,50)));
        p.drawText(cx-fm.width(QString::number(ly+=1))-2, i+2.6*scale/2 , QString::number(ly));
    }


    for(int i = 0; i<con->rods.size(); i++)
    {



        QPen pen(QColor(255,255,255,150));
        pen.setWidth(2);
        p.setPen(pen);
        p.drawLine(cx+scale*con->nodes[con->rods[i].n1-1].x/10,
                cy-scale*con->nodes[con->rods[i].n1-1].y/10,
                cx+scale*con->nodes[con->rods[i].n2-1].x/10,
                cy-scale*con->nodes[con->rods[i].n2-1].y/10);

        if(drawN)
        {

            pen.setWidth(1);
            p.setPen(pen);

            p.setFont(QFont("Arial",scale));
            QFontMetrics fm(QFont("Arial",scale));

            float nrx = (cx+scale*con->nodes[con->rods[i].n1-1].x/10+cx+scale*con->nodes[con->rods[i].n2-1].x/10)/2;
            float nry = (cy-scale*con->nodes[con->rods[i].n1-1].y/10+cy-scale*con->nodes[con->rods[i].n2-1].y/10)/2;
            p.drawRect(nrx-(fm.width(QString::number(i))+2)/2, nry-fm.height()/2, fm.width(QString::number(i))+2,fm.height());

            int n = i+1;

            p.setPen(QPen(QColor(255,255,255,150)));
            p.drawText(nrx-fm.width(QString::number(n))/2, nry+scale/2 , QString::number(n));
        }
    }


    if(drawDistF)
    {
        QImage arrow(3*scale,1.6*scale,QImage::Format_ARGB32);
        arrow.fill(qRgba(0, 0, 0, 150));
        QPainter pa(&arrow);
        pa.setRenderHint(QPainter::Antialiasing, true);
        QPen penPa(QColor(0,0,255,150));
        penPa.setWidth(1.5);
        pa.setPen(penPa);
        pa.drawLine(0,arrow.height()/2-0.3*scale,arrow.width()-2*scale,arrow.height()/2-0.3*scale);
        pa.drawLine(0,arrow.height()/2+0.3*scale,arrow.width()-2*scale,arrow.height()/2+0.3*scale);
        pa.drawLine(arrow.width()-2*scale,arrow.height()/2-0.3*scale,arrow.width()-2*scale,0);
        pa.drawLine(arrow.width()-2*scale,arrow.height()/2+0.3*scale,arrow.width()-2*scale,arrow.height());
        pa.drawLine(arrow.width()-2*scale,0,arrow.width(),arrow.height()/2);
        pa.drawLine(arrow.width()-2*scale,arrow.height(),arrow.width(),arrow.height()/2);
        pa.end();




        for(int i=0; i<con->distLoads.size(); i++)
        {
            int length = qSqrt(qPow((con->nodes[con->rods[con->distLoads[i].rod-1].n2-1].x/10*scale-
                               con->nodes[con->rods[con->distLoads[i].rod-1].n1-1].x/10*scale),2)+
                    qPow((con->nodes[con->rods[con->distLoads[i].rod-1].n2-1].y/10*scale-
                    con->nodes[con->rods[con->distLoads[i].rod-1].n1-1].y/10*scale),2));

            double ang = atan2(con->nodes[con->rods[con->distLoads[i].rod-1].n2-1].y/10*scale-
                    con->nodes[con->rods[con->distLoads[i].rod-1].n1-1].y/10*scale,
                    con->nodes[con->rods[con->distLoads[i].rod-1].n2-1].x/10*scale-
                    con->nodes[con->rods[con->distLoads[i].rod-1].n1-1].x/10*scale)*180/M_PI;

            QImage distArrow(2.5*length, 2.5*length,QImage::Format_ARGB32);
            distArrow.fill(qRgba(0, 0, 0, 0));
            QPainter pda(&distArrow);
            pda.setRenderHint(QPainter::SmoothPixmapTransform, true);


            QTransform trans;
            trans.translate(distArrow.width()/2, distArrow.height()/2);
            trans.rotate(-ang);
            pda.setTransform(trans);


            for(int n = 0; n < length; n+=arrow.width())
            {
                if(con->distLoads[i].dir == 1)
                    pda.drawImage(n, -arrow.height()/2, arrow);
                else if(con->distLoads[i].dir == -1)
                    pda.drawImage(n, -arrow.height()/2, arrow.mirrored(true, false));

            }


            QFontMetrics fm(QFont("Arial",scale));

            QImage qimg(fm.width(QString::number(con->distLoads[i].q)),fm.height(),QImage::Format_ARGB32);
            qimg.fill(qRgba(0, 0, 0, 255));

            QPainter pqimg(&qimg);
            pqimg.setRenderHint(QPainter::Antialiasing, true);

            pqimg.setFont(QFont("Arial",scale));
            QPen penQimg(QColor(0,0,255,150));
            penQimg.setWidth(1);
            pqimg.setPen(penQimg);

            pqimg.drawText(0, fm.height()-0.2*scale, QString::number(con->distLoads[i].q));

            pqimg.end();

            QMatrix qimgRot;
            qimgRot.rotate(ang);
            qimg = qimg.transformed(qimgRot, Qt::SmoothTransformation);


            pda.drawImage(length/2-qimg.width()/2, 1.2*scale, qimg);


            pda.resetTransform();
            pda.end();


            p.drawImage(QRect(cx+con->nodes[con->rods[con->distLoads[i].rod-1].n1-1].x/10*scale-distArrow.width()/2,
                    cy-con->nodes[con->rods[con->distLoads[i].rod-1].n1-1].y/10*scale-distArrow.height()/2,
                    distArrow.width(),
                    distArrow.height()
                    ),distArrow);

        }

    }



    if(drawConF)
    {


        QImage arrow(3*scale,1.6*scale,QImage::Format_ARGB32);
        arrow.fill(qRgba(0, 0, 0, 255));
        QPainter pa(&arrow);
        QPen penPa(QColor(255,0,0,150));
        penPa.setWidth(1);
        pa.setPen(penPa);
        pa.drawLine(0,arrow.height()/2-0.3*scale,arrow.width()-2*scale,arrow.height()/2-0.3*scale);
        pa.drawLine(0,arrow.height()/2+0.3*scale,arrow.width()-2*scale,arrow.height()/2+0.3*scale);
        pa.drawLine(arrow.width()-2*scale,arrow.height()/2-0.3*scale,arrow.width()-2*scale,0);
        pa.drawLine(arrow.width()-2*scale,arrow.height()/2+0.3*scale,arrow.width()-2*scale,arrow.height());
        pa.drawLine(arrow.width()-2*scale,0,arrow.width(),arrow.height()/2);
        pa.drawLine(arrow.width()-2*scale,arrow.height(),arrow.width(),arrow.height()/2);
        pa.end();



        for(int i = 0; i<con->conLoads.size(); i++)
        {


            if(con->conLoads[i].dir == 1)
            {
                p.drawImage(QRect(cx+con->nodes[con->conLoads[i].node-1].x/10*scale+0.8*scale,
                            cy-con->nodes[con->conLoads[i].node-1].y/10*scale-arrow.height()/2,
                        3*scale,1.6*scale),
                        arrow);

                QPen pen(QColor(255,0,0,150));
                pen.setWidth(2);
                p.setPen(pen);

                p.setFont(QFont("Arial",scale));
                QFontMetrics fm(QFont("Arial",scale));

                pen.setColor(QColor(0,0,0,255));
                p.setPen(pen);

                p.drawRect(cx+con->nodes[con->conLoads[i].node-1].x/10*scale+arrow.width()/2-fm.width(QString::number(con->conLoads[i].f))/2,
                        cy-con->nodes[con->conLoads[i].node-1].y/10*scale+1.2*scale ,
                        fm.width(QString::number(con->conLoads[i].f)),
                        fm.height());

                pen.setColor(QColor(255,0,0,150));
                p.setPen(pen);

                p.drawText(cx+con->nodes[con->conLoads[i].node-1].x/10*scale+arrow.width()/2-fm.width(QString::number(con->conLoads[i].f))/2,
                        cy-con->nodes[con->conLoads[i].node-1].y/10*scale+2.4*scale , QString::number(con->conLoads[i].f));


            }else if(con->conLoads[i].dir == -1){
                p.drawImage(QRect(cx+con->nodes[con->conLoads[i].node-1].x/10*scale-arrow.width()-0.8*scale,
                            cy-con->nodes[con->conLoads[i].node-1].y/10*scale-arrow.height()/2,
                        3*scale,1.6*scale),
                        arrow.mirrored(true, false));


                QPen pen(QColor(255,0,0,150));
                pen.setWidth(2);
                p.setPen(pen);

                p.setFont(QFont("Arial",scale));
                QFontMetrics fm(QFont("Arial",scale));

                pen.setColor(QColor(0,0,0,255));
                p.setPen(pen);

                p.drawRect(cx+con->nodes[con->conLoads[i].node-1].x/10*scale-arrow.width()/2-fm.width(QString::number(con->conLoads[i].f))/2,
                        cy-con->nodes[con->conLoads[i].node-1].y/10*scale+1.2*scale ,
                        fm.width(QString::number(con->conLoads[i].f)),
                        fm.height());

                pen.setColor(QColor(255,0,0,150));
                p.setPen(pen);


                p.drawText(cx+con->nodes[con->conLoads[i].node-1].x/10*scale-arrow.width()/2-fm.width(QString::number(con->conLoads[i].f))/2,
                        cy-con->nodes[con->conLoads[i].node-1].y/10*scale+2.4*scale , QString::number(con->conLoads[i].f));

            }

        }
    }

    for(int i = 0; i<con->nodes.size(); i++)
    {
        QPen pen(QColor(255,255,255,150));
        pen.setWidth(2);
        p.setPen(pen);
        p.drawEllipse(cx+scale*con->nodes[i].x/10-1*scale, cy-scale*con->nodes[i].y/10-1*scale,2*scale,2*scale);

        if(drawN)
        {

            pen.setWidth(1);
            p.setPen(pen);

            p.setFont(QFont("Arial",scale));
            QFontMetrics fm(QFont("Arial",scale));

            //p.drawEllipse(cx+scale*con->nodes[i].x/10-(fm.width(QString::number(i))+8)/2,
            //              cy-scale*con->nodes[i].y/10-(fm.width(QString::number(i))+8)/2,
            //              fm.width(QString::number(i))+8,fm.width(QString::number(i))+8);

            int n = i+1;

            p.setPen(QPen(QColor(255,255,255,150)));
            p.drawText(cx+scale*con->nodes[i].x/10-fm.width(QString::number(n))/2, cy-scale*con->nodes[i].y/10+scale/2 , QString::number(n));

        }
    }

    QImage prop(1.2*scale, 4*scale,QImage::Format_ARGB32);
    prop.fill(qRgba(0, 0, 0, 255));
    QPainter paProp(&prop);
    paProp.setRenderHint(QPainter::Antialiasing);
    QPen pPen(QColor(0,255,0,150));
    pPen.setWidth(2);
    paProp.setPen(pPen);

    paProp.drawLine(prop.width()-1,0,prop.width()-1,prop.height());

    for(int i = 0; i < prop.height(); i+=0.5*scale+0.5)
    {
        paProp.drawLine(prop.width()-1,i,prop.width()-scale,i+0.5*scale+0.5);
    }

    paProp.end();

    for(int i = 0; i < con->props.size(); i++)
    {
        if(con->props[i].dir == -1)
        {
        p.drawImage(cx+con->nodes[con->props[i].node-1].x/10*scale-prop.width(),
                    cy-con->nodes[con->props[i].node-1].y/10*scale-prop.height()/2,
                    prop);
        }else if(con->props[i].dir == 1){
            p.drawImage(cx+con->nodes[con->props[i].node-1].x/10*scale,
                        cy-con->nodes[con->props[i].node-1].y/10*scale-prop.height()/2,
                        prop.mirrored(true, false));
        }
    }



}

void Painter::keyPressEvent(QKeyEvent *k)
{
    if(k->key() == Qt::Key_Up)
        ofy+=100/scale;
    if(k->key() == Qt::Key_Down)
        ofy-=100/scale;
    if(k->key() == Qt::Key_Right)
        ofx-=100/scale;
    if(k->key() == Qt::Key_Left)
        ofx+=100/scale;

    if(k->key() == Qt::Key_Plus)
        scale+=1;

    if(k->key() == Qt::Key_Minus)
        if(scale > 1) scale-=1;


    repaint();
}

void Painter::mousePressEvent(QMouseEvent *)
{
    setFocus();
}
