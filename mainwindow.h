#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>
#include <QHBoxLayout>
#include <QTabWidget>
#include <QVBoxLayout>
#include <QScrollArea>
#include <QTableWidget>
#include <QLabel>
#include <QPushButton>
#include <QCheckBox>
#include <QDebug>
#include <QToolBar>
#include <QMessageBox>
#include <QFileDialog>


#include "painter.h"
#include "construction.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    QListWidgetItem *preProcessorItem;
    QListWidgetItem *processorItem;
    QListWidgetItem *postProcessorItem;

    QHBoxLayout *bodyLayout;

    void setLeftMenu();

    QTabWidget *tabsPreProc;
    Painter *drawCon;
    void setPreProcBody();

    QWidget *tab2;
    QWidget *tab2Body;
    QTableWidget *nodesTable;
    QPushButton *addNode;
    QPushButton *removeNode;

    QTableWidget *rodTable;
    QPushButton *addRod;
    QPushButton *removeRod;

    QTableWidget *rodTypeTable;
    QPushButton *addRodType;
    QPushButton *removeRodType;
    void setConTab2();
    void updateConTab2();

    QWidget *tab3;
    QWidget *tab3Body;
    QTableWidget *conLoadTable;
    QPushButton *addConLoad;
    QPushButton *removeConLoad;

    QTableWidget *distLoadTable;
    QPushButton *addDistLoad;
    QPushButton *removeDistLoad;

    QTableWidget *propTable;
    QPushButton *addProp;
    QPushButton *removeProp;
    void setConTab3();
    void updateConTab3();



    Construction con;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void tabChange(int tab);

    void addNodeButtonClicked();
    void removeNodeButtonClicked();

    void addRodButtonClicked();
    void removeRodButtonClicked();

    void addRodTypeButtonClicked();
    void removeRodTypeButtonClicked();

    void addConLoadButtonClicked();
    void removeConLoadButtonClicked();

    void addDistLoadButtonClicked();
    void removeDistLoadButtonClicked();

    void addPropButtonClicked();
    void removePropButtonClicked();

    void saveFile();
    void openFile();
    void aboutProgramm();


protected:
    void resizeEvent(QResizeEvent *e);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
